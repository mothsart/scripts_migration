# Scripts de migration


## Convertir un fichier .conf (pickle) en .yaml (configuration des handymenus)

Exemple :

```bash
  python pickleToYaml.py tests/handymenu_maxi.conf destination/handymenu.yaml
```

Ce script est compatible python 2 et 3.

## Migration des Handymenus

Les nouveaux Handymenus présents à partir de la Primtux4 utilise un fichier Yaml pour sa configuration.
(Ce qui simplifie grandement son édition comparé à un fichier binaire)

Ce script, non bloquant (s'il rencontre un soucis, il le signalera mais passera à la suite), va :

1. Convertir les fichiers .conv provenant des 4 sessions par défaut en une forme textuelle au format Yaml
2. Enregistrer le résultat dans /etc/handymenu/handymenu-{lasession}.yaml
3. Enregistrer le résultat dans le squelette de la session. 
Par exemple, pour le prof (administrateur) ce sera /etc/skel/handymenu.yaml
Pour l'utilisateur mini, se sera /etc/skel-mini/handymenu.yaml

Il est à préciser qu'il faudra lancer ce script avec les privilèges root (par exemple avec sudo).

```bash
  python handymenuMigration.py
```

Pour avoir plus de détails d'erreurs, il est possible d'activer le mode verbeux

```bash
  python handymenuMigration.py -v
```

ou 

```bash
  python handymenuMigration.py --verbose
```
